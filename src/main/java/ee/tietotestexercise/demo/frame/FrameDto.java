package ee.tietotestexercise.demo.frame;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class FrameDto {

  private int number;
  private int score;
  private List<Integer> points;

}
