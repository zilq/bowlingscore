package ee.tietotestexercise.demo.frame;

import ee.tietotestexercise.api.Frame;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface FrameConverter {

  @Mappings({
    @Mapping(target = "number", source = "number"),
    @Mapping(target = "points", source = "points")
  })
  Frame convert(FrameDto frameDto);

  @Mappings({
    @Mapping(source = "number", target = "number"),
    @Mapping(source = "points", target = "points")
  })
  FrameDto convert(Frame frame);

  default List<FrameDto> convert(List<Frame> frames) {
    return frames.stream()
      .map(this::convert)
      .collect(Collectors.toList());
  }
}
