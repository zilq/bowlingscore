package ee.tietotestexercise.demo.player;

import ee.tietotestexercise.api.BowlingScore;
import ee.tietotestexercise.api.Player;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PlayerService {

  private final BowlingScore bowlingScore;
  private final PlayerConverter converter;

  public PlayerDto addPlayer(String name) {
    Player player = Player.of(name);
    bowlingScore.addPlayer(player);
    return converter.convert(player);
  }

  public void removePlayer(int id) {
    bowlingScore.removePlayer(id);
  }

  public void removeAllPlayers() {
    bowlingScore.removeAllPlayers();
  }

}
