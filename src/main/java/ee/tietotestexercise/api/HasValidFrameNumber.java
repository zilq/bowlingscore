package ee.tietotestexercise.api;

import java.util.List;

class HasValidFrameNumber implements ValidationRule {

  @Override
  public void validate(Frame frame, List<Frame> frames, List<String> errors) {
    if (frame.getNumber() < Frame.FIRST_FRAME || frame.getNumber() > Frame.LAST_FRAME) {
      errors.add(String.format("Invalid frame number. Valid numbers range from %d to %d.",
        Frame.FIRST_FRAME, Frame.LAST_FRAME));
    }
  }

}
