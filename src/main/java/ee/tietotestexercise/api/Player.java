package ee.tietotestexercise.api;

public class Player {

  private static int nextId = 0;
  private final int id;
  private final String name;

  private Player(String name) {
    id = nextId++;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }

  public static Player of(String name) {
    return new Player(name);
  }

  @Override
  public String toString() {
    return String.format("PLAYER:\nID: %d, NAME: %s\n", id, name);
  }

}
