package ee.tietotestexercise.api;

import java.util.*;

import static ee.tietotestexercise.api.FrameType.SPARE;
import static ee.tietotestexercise.api.FrameType.STRIKE;

class FrameRepository {

  private Map<Integer, List<Frame>> frames;
  private Map<Integer, List<Frame>> incompleteFrames;
  private List<Integer> playerIds;
  private FrameValidator frameValidator;

  FrameRepository() {
    frames = new HashMap<>();
    incompleteFrames = new HashMap<>();
    playerIds = new ArrayList<>();
    frameValidator = new FrameValidator();
  }

  void initPlayerFrames(int playerId) {
    if (!frames.containsKey(playerId)) {
      frames.put(playerId, new ArrayList<>());
      incompleteFrames.put(playerId, new ArrayList<>());
      playerIds.add(playerId);
    }
  }

  void initPlayerFrames(List<Integer> playerIds) {
    playerIds.forEach(this::initPlayerFrames);
  }

  void removePlayerFrames(int playerId) {
    frames.remove(playerId);
    incompleteFrames.remove(playerId);
    playerIds.remove(playerId);
  }

  void removeAllFrames() {
    frames.clear();
    incompleteFrames.clear();
  }

  List<Frame> getFrames(int playerId) {
    return frames.get(playerId);
  }

  void addFrame(Frame frame, int playerId) {
    if (!isPlayerTurn(playerId, frame.getNumber())) {
      throw new IllegalArgumentException("Invalid turn. Received frame for player (ID: " + playerId + ") out of turn.");
    }

    List<Frame> frames = this.frames.get(playerId);
    frameValidator.validate(frame, frames);
    frames.add(frame);
    finalizeIncompleteFrames(playerId);
    if (frame.getNumber() == Frame.LAST_FRAME) {
      finalizeLastFrame(frame);
    } else if (frame.getScore() == Frame.MAX_POINTS_FROM_SINGLE_TURN) {
      incompleteFrames.get(playerId).add(frame);
    }
  }

  boolean isPlayerTurn(int playerId, int frameNr) {
    int currentPlayerIdx = playerIds.indexOf(playerId);
    boolean isFirstPlayer = currentPlayerIdx == 0;
    int previousPlayerId = isFirstPlayer ? playerIds.get(playerIds.size() - 1) : playerIds.get(currentPlayerIdx - 1);
    int previousPlayerFramesPlayed = frames.get(previousPlayerId).size();

    return playerIds.size() == 1
      || isFirstPlayer && previousPlayerFramesPlayed == frameNr - 1
      || !isFirstPlayer && previousPlayerFramesPlayed == frameNr;
  }

  private void finalizeIncompleteFrames(int playerId) {
    List<Frame> incompleteFrames = this.incompleteFrames.get(playerId);
    if (incompleteFrames.isEmpty()) {
      return;
    }

    List<Frame> finalizedFrames = new ArrayList<>();
    incompleteFrames.stream()
      .filter(incompleteFrame -> isFrameCompletable(incompleteFrame, playerId))
      .forEach(incompleteFrame -> {
        finalizeFrame(incompleteFrame, playerId);
        finalizedFrames.add(incompleteFrame);
      });
    incompleteFrames.removeAll(finalizedFrames);
  }

  private boolean isFrameCompletable(Frame frame, int playerId) {
    int frameNumber = frame.getNumber();
    FrameType type = frame.getType();
    Optional<Frame> nextFrame = getFrame(playerId, frameNumber + 1);

    if (nextFrame.isEmpty()) {
      return false;
    }

    int framesPlayed = getNumberOfFramesPlayed(playerId);
    int chancesUsedInNextFrame = nextFrame.get().getPoints().size();

    return (type.equals(STRIKE) && STRIKE.chancesToAddToFrameScore <= chancesUsedInNextFrame
      || (frameNumber + 2) <= framesPlayed) || (type.equals(SPARE) && framesPlayed > frameNumber);
  }

  private void finalizeLastFrame(Frame frame) {
    frame.updateScore(frame.getPoints().stream().reduce(0, Integer::sum));
  }

  private Optional<Frame> getFrame(int playerId, int number) {
    return frames.get(playerId).stream()
      .filter(frame -> frame.getNumber() == number)
      .findFirst();
  }

  private int getNumberOfFramesPlayed(int playerId) {
    return frames.get(playerId).size();
  }

  private void finalizeFrame(Frame frame, int playerId) {
    int chancesToAdd = frame.getType().chancesToAddToFrameScore;
    int frameNumber = frame.getNumber();
    int score = 0;
    for (int i = 1; i <= 2; i++) {
      Frame nextFrame = getFrame(playerId, frameNumber + i).orElseThrow(() -> new IllegalStateException());
      List<Integer> points = nextFrame.getPoints();
      int limit = Math.min(chancesToAdd, points.size());
      for (int j = 0; j < limit; j++) {
        score += points.get(j);
      }
      if (points.size() >= chancesToAdd) {
        break;
      }
      chancesToAdd--;
    }
    frame.updateScore(frame.getScore() + score);
  }


  public Map<Integer, List<Frame>> getAllFrames() {
    return frames;
  }

  public void reset() {
    playerIds.forEach(id -> {
      frames.get(id).clear();
      incompleteFrames.get(id).clear();
    });
  }

  public Map<Integer, Integer> getScores() {
    Map<Integer, Integer> scores = new HashMap<>();
    frames.keySet().forEach(key -> {
      List<Frame> playerFrames = frames.get(key);
      int score = playerFrames.stream()
        .map(Frame::getScore)
        .reduce(0, Integer::sum);
      scores.put(key, score);
    });

    return scores;
  }
}
