package ee.tietotestexercise.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class FrameRepositoryTest {

  private static final int PLAYER_ID = 1;
  private static FrameRepository repository;

  @BeforeEach
  void setUp() {
    repository = new FrameRepository();
    initPlayerFrames(PLAYER_ID);
  }

  @Test
  void spareIsCompletableAfterOneNewChance() {
    Frame spare = new Frame(1, List.of(1, 9));
    Frame second = new Frame(2, List.of(2, 5));
    Frame third = new Frame(3, List.of(10));
    addFrames(List.of(spare, second, third), PLAYER_ID);

    assertEquals(12, spare.getScore());
  }

  @Test
  void strikeIsCompletableAfterTwoChances() {
    Frame strike = new Frame(1, List.of(10));
    Frame second = new Frame(2, List.of(5, 2));
    Frame third = new Frame(3, List.of(3, 7));
    addFrames(List.of(strike, second, third), PLAYER_ID);

    assertEquals(17, strike.getScore());
  }

  @Test
  void spareIsCompletableAfterFirstStrike() {
    Frame spare = new Frame(1, List.of(2, 8));
    Frame strike = new Frame(2, List.of(10));
    addFrames(List.of(spare, strike), PLAYER_ID);

    assertEquals(20, spare.getScore());
  }

  @Test
  void strikeIsCompletableAfterTwoStrikes() {
    Frame strike = new Frame(1, List.of(10));
    Frame second = new Frame(2, List.of(10));
    Frame third = new Frame(3, List.of(10));
    addFrames(List.of(strike, second, third), PLAYER_ID);

    assertEquals(30, strike.getScore());
  }

  @Test
  void strikeIsCompletableAfterStrikeAndOneChance() {
    Frame strike = new Frame(1, List.of(10));
    Frame second = new Frame(2, List.of(10));
    Frame third = new Frame(3, List.of(3, 5));
    addFrames(List.of(strike, second, third), PLAYER_ID);

    assertEquals(23, strike.getScore());
  }

  @Test
  void playerWithNoFramesPlayedHasScoreOfZero() {
    assertEquals(0, repository.getScores().get(PLAYER_ID));
  }

  @Test
  void playerWithOneFramePlayedHasCorrectScore() {
    addFrames(List.of(new Frame(1, List.of(3, 5))), PLAYER_ID);
    assertEquals(8, repository.getScores().get(PLAYER_ID));
  }

  @Test
  void playerWithAllGutterBallsHasScoreOfZero() {
    List<Frame> frames = IntStream.rangeClosed(1, 10)
      .boxed()
      .map(i -> new Frame(i, List.of(0, 0)))
      .collect(Collectors.toList());
    addFrames(frames, PLAYER_ID);
    assertEquals(0, repository.getScores().get(PLAYER_ID));
  }

  @Test
  void playerWithAllStrikesHasScoreOf300() {
    List<Frame> frames = IntStream.range(1, 10)
      .boxed()
      .map(i -> new Frame(i, List.of(10)))
      .collect(Collectors.toList());
    frames.add(new Frame(10, List.of(10, 10, 10)));
    addFrames(frames, PLAYER_ID);
    assertEquals(300, repository.getScores().get(PLAYER_ID));
  }

  @Test
  void addingPlayerValidFrameOutOfTurnThrows() {
    int secondPlayerId = PLAYER_ID + 1;
    int thirdPlayerId = PLAYER_ID + 2;
    initPlayerFrames(secondPlayerId);
    initPlayerFrames(thirdPlayerId);
    addFrames(List.of(new Frame(1, List.of(5, 2))), PLAYER_ID);
    assertThrows(IllegalArgumentException.class,
      () -> addFrames(List.of(new Frame(1, List.of(10))), thirdPlayerId));
  }

  @Test
  void firstPlayerNewValidFrameDoesNotThrow() {
    int secondPlayerId = PLAYER_ID + 1;
    initPlayerFrames(secondPlayerId);
    addFrames(List.of(new Frame(1, List.of(5, 2))), PLAYER_ID);
    addFrames(List.of(new Frame(1, List.of(10))), secondPlayerId);
    assertDoesNotThrow(() -> addFrames(List.of(new Frame(2, List.of(2, 3))), PLAYER_ID));
  }

  @Test
  void singlePlayerTwoFramesInARowDoesNotThrow() {
    List<Frame> frames = List.of(new Frame(1, List.of(10)), new Frame(2, List.of(2, 5)));
    assertDoesNotThrow(() -> addFrames(frames, PLAYER_ID));
  }

  @Test
  void multiPlayerTwoFramesInARowThrows() {
    int secondPlayerId = PLAYER_ID + 1;
    initPlayerFrames(secondPlayerId);
    List<Frame> frames = List.of(new Frame(1, List.of(10)), new Frame(2, List.of(2, 5)));
    assertThrows(IllegalArgumentException.class, () -> addFrames(frames, PLAYER_ID));
  }

  private void initPlayerFrames(int playerId) {
    repository.initPlayerFrames(playerId);
  }

  private void addFrames(List<Frame> frames, int playerId) {
    frames.forEach(frame -> repository.addFrame(frame, playerId));
  }


}