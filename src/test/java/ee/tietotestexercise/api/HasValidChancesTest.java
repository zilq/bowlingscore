package ee.tietotestexercise.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HasValidChancesTest {

  private static ValidationRule rule;
  private static List<String> errors;

  @BeforeEach
  void setUp() {
    rule = new HasValidChances();
    errors = new ArrayList<>();
  }

  @Test
  void frameWithNoChancesHasErrors() {
    Frame frame = new Frame(1, List.of());
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void nonFinalFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(1, List.of(0, 0, 0));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(10, List.of(0, 0, 0, 0));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void nonFinalOpenFrameWithCorrectChancesHasNoErrors() {
    Frame frame = new Frame(1, List.of(0, 9));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void nonFinalSpareFrameWithCorrectChancesHasNoErrors() {
    Frame frame = new Frame(1, List.of(1, 9));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void nonFinalStrikeFrameWithCorrectChancesHasNoErrors() {
    Frame frame = new Frame(1, List.of(10));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void nonFinalOpenFrameWithTooFewChancesHasErrors() {
    Frame frame = new Frame(1, List.of(5));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void nonFinalOpenFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(1, List.of(1, 1, 3));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void nonFinalSpareFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(1, List.of(1, 1, 8));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void nonFinalStrikeFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(1, List.of(10, 2));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalOpenFrameWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(0, 9));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalSpareAndStrikeWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(0, 10, 10));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalSpareWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(0, 10, 8));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalOneStrikeWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(10, 0, 0));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalTwoStrikesWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(10, 10, 0));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalFirstAndThirdChanceStrikesWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(10, 0, 10));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalThreeStrikesWithCorrectChancesNoErrors() {
    Frame frame = new Frame(10, List.of(10, 10, 10));
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void finalOpenFrameWithTooFewChancesHasErrors() {
    Frame frame = new Frame(10, List.of(0));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalOpenFrameWithTooManyChancesHasErrors() {
    Frame frame = new Frame(10, List.of(0, 0, 0));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalSpareFrameWithTooFewChancesHasErrors() {
    Frame frame = new Frame(10, List.of(0, 10));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalOneStrikeWithTooFewChancesHasErrors() {
    Frame frame = new Frame(10, List.of(10));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void finalTwoStrikeWithTooFewChancesHasErrors() {
    Frame frame = new Frame(10, List.of(10, 10));
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

}