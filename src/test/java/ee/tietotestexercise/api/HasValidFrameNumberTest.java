package ee.tietotestexercise.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HasValidFrameNumberTest {

  private static ValidationRule rule;
  private static List<String> errors;

  @BeforeEach
  void setUp() {
    rule = new HasValidFrameNumber();
    errors = new ArrayList<>();
  }

  @Test
  void correctFrameNumberNoErrors() {
    Frame frame = createFrame(1);
    rule.validate(frame, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void frameNumberTooLowValidationHasError() {
    Frame frame = createFrame(0);
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  @Test
  void frameNumberTooHighValidationHasErrors() {
    Frame frame = createFrame(11);
    rule.validate(frame, List.of(), errors);
    assertFalse(errors.isEmpty());
  }

  private Frame createFrame(int number) {
    return new Frame(number, List.of(0, 0));
  }

}